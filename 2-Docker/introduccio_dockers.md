### Tecnologia DOCKER

+ [Introducció a dockers per Xavier Sala](docker-presentacioXavierSala.pdf)

+ Podem definir com a funció principal dels dockers l'automatització de la tasca de desplegament d'aplicacions. Quan un desenvolupador vol provar el software que ha creat, necessita un entorn real (productiu) a on configurar, instal·lar i provar aquest software. Els containers ofereixen virtualització, simulant l'entorn real, tal i com ho faria una màquina virtual però fent servir menys recursos.

+ Per a nosaltres el més important serà diferenciar els conceptes de **imatge** i **contenidor**.

	- **Imatge**: un fitxer / plantilla a on es guarda tota la informació necessària per a construir contenidors. És READ ONLY.
	- **Contenidor**: és un entorn aïllat a on s'executaran aplicacions. Un contenidor és una execució de la imatge.
	I com podem executar una imatge diverses vegades, d'una sola imatge podem crear diversos contenidors.

#### Instal·lació infraestructura dockers

Seguint les indicacions que es troben a la website de docker, prepararem les eines adequades en diferents fases.

##### 0-Desinstal·lació de versions antigues

Desinstal·larem qualsevol versió de docker prèvia que tinguem. Si no hi havia
res instal·lat no passarà res:

```
sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine
```

Aquesta desinstal·lació no elimina els possibles fitxers (imatges, dockers,
...) que es trobin a `/var/lib/docker`

##### 1-Instal·lació de Docker Engine community

Per poder gestionar els repositoris des de la línia d'ordres necessitem
`dnf-plugins-core` que potser ja el tenim instal·lat:

```
sudo dnf -y install dnf-plugins-core
```

Afegim el repositori estable (existeixen versions més noves i per tant
*arriscades*):

```
sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
```

Desprès d'haver configurat el repositori, instal·lem *docker engine*:

```
sudo dnf install docker-ce docker-ce-cli containerd.io

```

Si se'ns pregunta si acceptem la clau GPG, verifiquem que coincideix amb

```
060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
```

i, en cas afirmatiu, acceptem.

##### 2-Arrencada del docker engine

Docker engine està instal·lat però no engegat. Com sempre `systemctl` ens ajudarà:

```
sudo systemctl start docker
```

Si volem que el servei de docker s'activi sempre a l'arrencar el nostre sistema
haurem d'utilitzar el paràmetre `enable`.

##### 3-Post-instal·lació

A l'instal·lar docker engine s'ha creat un *grup* de nom `docker`. Si volem que el
nostre usuari ordinari pugui executar docker sense necessitat de fer servir
`sudo` o sense necessitat de ser root, podem afegir el nostre usuari al grup
docker, per exemple amb l'ordre:

```
sudo gpasswd -a $(whoami) docker
```

Perquè tingui efecte aquest canvi, necessitem sortir de la sessió i tornar entrar.

##### 4-Verificació

Comprovem que efectivament funciona arrencant la imatge `hello-world`:

```
docker run hello-world
```

##### 5-Baixada de la imatge de Docker per fer els exercicis de l'Activitat 3

Primer necessitem tenir una imatge, aquesta la podem tenir en el nostre
repositori local, i si el sistema no la troba la buscarà a les imatges
públiques de *[Docker Hub](https://hub.docker.com/)*. Una altra opció es
crear-se un compte a on tenir les nostres imatges, que podem fer privades o
públiques.

Al nostre cas, ja tenim preparada una imatge bàsica de Fedora 27 (sense entorn
gràfic) amb les eines necessàries per poder jugar amb les ordres bàsiques de
xarxes.

Ens baixem la imatge:

``` 
docker run --cap-add=NET_ADMIN  --name pc01 -h pc01 -it jamoros/fedora:software_xarxes /bin/bash
```

*run*: crea un container des d'una imatge

*--cap-add=NET_ADMIN*: permisos especials d'administració (per exemple, per
poder parar/ engegar dispositius de xarxa)

*--name*: nom que li posem al nostre contenidor (CONTAINER)

*-h*: hostname

*-it*: sessió interactiva, la *i* ens proporciona una entrada stdin i la *t*
una pseudo-terminal tty (I si no fem servir aquesta opció? Moltes vegades és
útil que un contenidor faci de servidor, però sense necessitat d'accedir a ell
de forma interactiva) 

*jamoros/fedora:software_xarxes*: nom de la imatge

`/bin/bash` Moltes vegades no és necessari perquè la imatge ja té aquest
programa però si no el té l'haurem de passar.

Podem llistar les imatges que tenim al repositori local amb:

```
docker images
```

Podem llistar els contenidors amb:

```
docker ps
```

Per defecte, l'ordre anterior ens mostrarà només els contenidors que estiguin en execució en aquest moment, però si volem veure'ls tots, inclosos els que estan parats, afegirem l'opció `-a`

Per engegar un container de nom pc01:

```
docker start -a pc01
```

Si ens oblidem de la `-a` de attached, haurem arrencat el container però no tindrem accés a ell, necessitarem després fer el *attach* amb la següent ordre:

```
docker attach pc01
```

Si hem instal·lat software nou a un contenidor, això només romandrà al contenidor, no a la imatge. Però si considerem que pot ser interessant fer una plantilla, podem fer un commit del contenidor per crear una nova imatge!

```
docker commit --help
```


#### LINKS

+ [Documentació de dockers utilitzant Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)

+ [Documentació de dockers a RedHat](https://www.redhat.com/es/topics/containers/what-is-docker)

