## Exercicis introducció DOCKER

##### Exercici 1

A partir del que s'ha fet al document d'introducció a docker, crea 5 containers més de noms pc02, pc03, pc04, pc05, pc06, però amb una única ordre. Fes servir un `for` al bash i tampoc estaria de més fer servir {1..3}, {a..z} {900..999}).
Abans de fer-ho, creus que es baixarà 5 vegades la imatge de dockerhub `jamoros/fedora:software_xarxes` ?

##### Exercici 2

Com s'aconsegueix entrar en un container del qual estem "detached" ? Escriu i executa una ordre de manera que puguis accedir al pc01.

##### Exercici 3

Un cop estem dintre podem executar ordres com per exemple:

```
[root@pc01 /]# echo $HOSTNAME 
pc01
[root@pc01 /]# ip a show dev eth0
4: eth0@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

però sense necessitar d'entrar en cada una de les màquines una a una, com podríem automatitzar les ordres anteriors per als 6 pc's?

##### Exercici 4

- Escolliu ara dos contenidors, per exmple pc01 i pc02, per poder fer-los servir un de servidor de ftp i l'altre de client ftp.

- Quin servidor ftp és el clàssic que s'utiliza en entorns linux? Quin és el client ftp més habitual? Instal·leu-los a les màquines respectives.

- Aquest contenidors linux que teniu són molt bàsics, tant que no existeix l'ordre `systemctl` i per tant no podreu engegar el servidor `ftp` tal i com heu fet altres vegades. Haureu de trobar on està l'executable i executar-lo.

- Com penseu parar el servei quan feu algun canvi i necessiteu reiniciar-lo ?

- Finalment feu una prova senzilla, poseu un fitxer amb un missatge breu al servidor ftp, connecteu-vos des del client anònimament per descarregar-lo.

(No configureu la resolució de noms interna i feu servir les IP's de les màquines per connectar-vos)

##### Exercici 5

El que hem vist funciona bé si el client del servei és un altre container o el
mateix host, ja que tots estan dintre de la xarxa per defecte 172.17.0.1/16,
però com ho faríem per accedir des de un altre pc de la xarxa?

La tecnologia docker ens proporciona una solució molt senzilla.

Primer partirem de la imatge que ja teniu i després farem servir una que ja té el servidor instal·lat

Per fer això necessitem que respongueu a algunes preguntes:

- Necessitem mapejar els ports pel servei FTP quan creem el container a partir
  d'una imatge. Quina és l'opció?

- Si volem mapejar un directori concret (el dels fitxers que volem exportar per
  ftp) amb quina instrucció es fa?

- Primer fem un container interactiu, instal·leu el que calgui i manualment
  engegarem el servei. Després ens connectem des d'un altre ordinador de la LAN
o el mòbil.

Ara crearem un container des de la imatge `jamoros/fedora:vsftpd` que ja té
instal·lat un vsftpd, però heu d'aconseguir que arrenqui en background, per
fer això hi ha opcions que abans afegieu a l'ordre i que ara no hi seran.

- Quina serà l'opció que aconseguirà això?

- De quina altra manera es podrà automatitzar aquesta tasca? Feu un cop d'ull a les diapos de Xavier Sala


Comproveu cada una de les respostes de manera pràctica.



###### OBS: Recordeu sempre que estigui executant-se el servei docker i que l'usuari habitual que feu servir pertanyi al grup `docker`.

 



