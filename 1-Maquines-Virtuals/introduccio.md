### Introducció a les Màquines Virtuals

##### Algunes solucions conegudes

Si volem veure una comparativa completa d'una gran part de les solucions que ofereix el mercat, com sempre un bon lloc és [la wikipedia](https://en.wikipedia.org/wiki/Comparison_of_platform_virtualization_software)

Però un resum-esquema de les solucions més conegudes podria ser aquest:

```
                          +--------------------------------------+
                          |         MÀQUINES VIRTUALS            |
                          +-+----------+--------------+-----+----+
                            |          |              |     |
                            |          |              |     +----------------------------------+
        +-------------------+          |              |                                        |
        |                              |              |                                        |
        |                              |              +-------------------+                    |
        |                              |                                  |                    |
        |                          +---+---+                              |                    |
        |                          | Linux |                              |                    |
        |                          ++----+-+                              |                    |
        |                           |    |                                |                    |
        |                   +-------+    +--------+                       |                    |
        |                   |                     |                       |                    |
 +------v------+         +--v--+               +--v--+             +------v------+     +-------v-----+
 | Virtual Box |         | KVM¹|               | Xen |             |   VMware    |     |   Hyper_V   |
 +-------------+         +-----+               +-----+             +-------------+     +-------------+
 Oracle, lliure?     Kernel based               Cytrix            Programari privatiu  Microsoft, privatiu
 toy?                Lliure, Red Hat          vdi like isard      Líder petita i       poca quota mercat
                            +                                     mitjana empresa
                            |
                            | libvirt²
        +------------------------------------------+
        |                   |                      |
        |                   |                      |
+-------+------+        +---+---+            +-----+------+
| virt_manager |        | oVirt |            | Open Stack |
+--------------+        +-------+            +------------+
Interfaz gràfica       Solució petita i      Líder empreses grans (NASA, +500)
gestió VM's            mitjana empresa       Orientat a cloud (Amazon WS, OVH ...)
                                             IaaS³

```

----------------

Hi ha uns petits superíndex amagats al diagrama, cadascú d'aquests es correspón amb una imatge:

¹ Els següents diagrames mostren l'entorn KVM a alt nivell, on apareix Qemu, l'emulador de cpu:

![entorn KVM a alt nivell](Kernel-based_Virtual_Machine.png)

![diagrama KVM/QEMU](pila_KVM_QEMU.png)
----------

² El diagrama de sota mostra la interacció de la llibreria libvirt que permet gestionar diferents tecnologies de virtualització com KVM, Xen, VMware ESX, QEMU entre d'altres:

![interacció de la llibreria libvirt](libvirt_support.png)

-------------

³ També podem veure quin tipus de tecnologia és Open Stack (un IaaS: Infraestructure as a Service) des del següent diagrama de les capes que es defineixen al Cloud Computing:

![capes de cloud computing](Cloud_computing_layers.png)


