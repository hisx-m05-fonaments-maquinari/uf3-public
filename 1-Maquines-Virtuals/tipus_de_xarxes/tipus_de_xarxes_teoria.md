### Tipus de xarxes (NAT, bridge, aïllades ...)

Dues de les configuracions de xarxa més habituals són NAT ¹ i el *bridge*
(*dispositiu físic compartit*).


##### 0. Introducció: el switch virtual.

Per a la implementació de les xarxes virtuals *libvirtd* utilitza el concepte
de *switch virtual*

Un *switch virtual* és una implementació (per programari) d'un switch al qual
es poden connectar les VM. El trànsit de xarxa d'una VM es redirigeix
mitjançant aquest switch.

Un amfitrió Linux representa un *switch virtual* com una interfície de xarxa.

![switch virtual](vn-02-switchandtwoguests.png)

Quan s'instal·la i configura *libvirt*, la xarxa virtual per defecte s'anomena
*default*.

Aquest switch virtual està representat per la interfície *virbr0*:

```
[root@h31 ~]# ip addr show virbr0
3: virbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 52:54:00:a6:a9:4c brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
```

##### 1. Mode NAT

És l'opció que tindrem per defecte. El switch virtual operarà en mode NAT
(network address translation), concretament IP masquerading. 

IP masquerading permet a les màquines virtuals connectar-se cap a l'exterior
utilitzant l'**adreça IP de l'amfitrió**, però per defecte, les màquines de
l'exterior no podran connectar-se a les VM. 

![mode NAT](vn-04-hostwithnatswitch.png)

###### 1.1 DHCP i DNS

La configuració TCP/IP és pot assignar a les diferents VM's mitjançant DHCP.

Es pot assignar un pool d'adreces al switch virtual.

*libvirtd* inicia i configura una instància de *dnsmasq* para cada switch virtual. 

![switch with dnsmasq](vn-05-switchwithdnsmasq.png)

La configuració dels rangs de DHCP és molt senzilla utilitzant `virt-manager`:

![configuració pas 1 ](xarxa_DHCP_step_1.png)

![configuració pas 2 ](xarxa_DHCP_step_2.png)

##### 2. Mode Bridge

Quan s'utilitza el mode Bridge, les VM es troben dins de la mateixa xarxa
física que l'amfitrió. Totes les altres màquines físiques de la mateixa xarxa
física (la de l'amfitrió) detecten les VM i hi poden accedir. El mode bridge
treballa a la capa 2 del model de xarxa OSI.

![Mode bridge](vn-Bridged-Mode-Diagram.png)

Com que el mode NAT és el mode de xarxa que hi ha per defecte, per **establir
el mode bridge** haurem de fer alguns canvis en la configuració: 

![configuracio bridge macvtap](configuracio_bridge_macvtap_ethernet.png)

Pensem que s'haurà d'assignar adreces IP's de manera manual o, si n'hi ha DHCP
a la xarxa on està connectat l'amfitrió, de manera automàtica.

##### 3. Mode isolated (aïllades)

Quan s'utilitza el mode aïllat, les VM's connectades al switch virtual poden
comunicar-se entre si i amb la màquina física de l'amfitrió, però el seu
trànsit no passarà fora de la màquina física de l'amfitrió ni pot rebre trànsit
des de fora de la màquina física de l'amfitrió.


L'ús de *dnsmasq* en aquest mode és necessari per a funcionalitats bàsiques com
DHCP. Tanmateix, fins i tot si aquesta xarxa està aïllada de qualsevol xarxa
física, els noms de DNS encara es resolen. De manera que pot donar-se una
situació curiosa: es resolen els noms DNS, però fallen les comandes de la
sol·licitud d'echo ICMP (*ping*).

![mode aïllat](vn-07-isolated-switch.png)

Per utilitzar una xarxa aïllada, abans hem de crear-la, des de virt-manager és molt senzill:

![Creació d'una xarxa aïllada amb virt-manager](Creacio_xarxa_aillada.png)


##### LINKS:

* [wiki libvirt Networking](https://wiki.libvirt.org/page/Networking)
* [RedHat 7 Virtual Networking](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/chap-virtual_networking)
* [libvirt networking handbook](https://jamielinux.com/docs/libvirt-networking-handbook/)
* [isolated network](https://wiki.libvirt.org/page/TaskIsolatedNetworkSetupVirtManager)
* [isolated network](https://drive.google.com/file/d/1KaZdTQ08O5Yx_mGGUCczfk_DUXLob1Zh)


¹  El mode NAT de vegades també s'anomena *xarxa virtual* en contraposició al bridge que comparteix un dispositiu físic. També podem trobar l'expressió NAT bridge ( de fet tots els dispositius virtuals que mostra l'ordre ip són vir**br**_something ...)
